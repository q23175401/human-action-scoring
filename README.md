# 使用方法
使用 mediapipe_read_video.py 的 transform_video 就可以將一個影片轉換    
輸出的每一frame會到output的資料夾    


<br/>

# 需要的packages
``` 
pip install tensorflow, opencv, pathlib, tf2_yolov4, mediapipe
```

<br/>

# 測試可以直接跑
```
python mediapipe_read_video.py
```