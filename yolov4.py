import tensorflow as tf
import numpy as np
from tf2_yolov4.anchors import YOLOV4_ANCHORS
from tf2_yolov4.model import YOLOv4
import matplotlib.pyplot as plt
 
WIDTH, HEIGHT = (1024, 768)
CLASSES = [
    'person', 'bicycle',
     'car', 'motorcycle', 'airplane', 'bus', 'train', 'truck',
    'boat', 'traffic light', 'fire hydrant', 'stop sign', 'parking meter', 'bench',
    'bird', 'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant', 'bear', 'zebra',
    'giraffe', 'backpack', 'umbrella', 'handbag', 'tie', 'suitcase', 'frisbee',
    'skis', 'snowboard', 'ball', 'kite', 'baseball bat', 'baseball glove',
    'skateboard', 'surfboard', 'tennis racket', 'bottle', 'wine glass', 'cup', 'fork',
    'knife', 'spoon', 'bowl', 'banana', 'apple', 'sandwich', 'orange', 'broccoli',
    'carrot', 'hot dog', 'pizza', 'donut', 'cake', 'chair', 'couch', 'potted plant',
    'bed', 'dining table', 'toilet', 'tv', 'laptop',  'mouse', 'remote', 'keyboard',
    'cell phone', 'microwave', 'oven', 'toaster', 'sink', 'refrigerator', 'book',
    'clock', 'vase', 'scissors', 'teddy bear', 'hair drier', 'toothbrush'
]

class BallDetector():
    def __init__(self, im_h, im_w, im_d):
        self.im_h = im_h
        self.im_w = im_w
        self.im_d = im_d
        
        self.model = YOLOv4(
            input_shape=(HEIGHT, WIDTH, im_d),
            anchors=YOLOV4_ANCHORS,
            num_classes=len(CLASSES),
            training=False,
            yolo_max_boxes=50,
            yolo_iou_threshold=0.5,
            yolo_score_threshold=0.5,
        )

        self.model.load_weights('./weights/yolov4.h5')

    def predict(self, img):
        img = tf.image.resize(img, (HEIGHT, WIDTH))
        imgs = tf.expand_dims(img, axis=0) / 255

        boxes, scores, classes, detections = self.model.predict(imgs)
        boxes = boxes[0] * [self.im_w, self.im_h, self.im_w, self.im_h]
        scores = scores[0]
        classes = classes[0].astype(int)
        detections = detections[0]

        result_boxes = []
        for (xmin, ymin, xmax, ymax), score, class_idx in zip(boxes, scores, classes):
            if class_idx!=32 or score<=0:
                continue
            
            result_boxes.append((int(xmin), int(ymin), int(xmax), int(ymax)))
        return result_boxes
            
if __name__ == "__main__":
    image = tf.io.read_file('./output/testforyolo/frame_17.png')
    image = tf.io.decode_image(image)
    image = tf.image.resize(image, (HEIGHT, WIDTH))
    images = tf.expand_dims(image, axis=0) / 255

    bd = BallDetector(HEIGHT, WIDTH, 3)
    boxes = bd.predict(image)

    plt.imshow(images[0])
    ax = plt.gca()
    
    for (xmin, ymin, xmax, ymax) in boxes:
        rect = plt.Rectangle((xmin, ymin), xmax - xmin, ymax - ymin,
                                fill=False, color='green')
        ax.add_patch(rect)
        text = 'ball'
        ax.text(xmin, ymin, text, fontsize=9, bbox=dict(facecolor='yellow', alpha=0.6))
    
    plt.title('Objects detected')
    plt.axis('off')
    plt.show()