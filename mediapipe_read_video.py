from pathlib import Path
import matplotlib.pyplot as plt
import cv2
import mediapipe as mp
from yolov4 import BallDetector
import numpy as np
import pickle
mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles
mp_pose = mp.solutions.pose

def process_one_image(image, im_h, im_w, pose_model, ball_detector, output_filename, *, save=True):
    pose_result = pose_model.process(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))

    ball_boxes = ball_detector.predict(image)
    for (xmin, ymin, xmax, ymax) in ball_boxes:
        print((xmin, ymin, xmax, ymax), end='\t')
        image = cv2.rectangle(image, (xmin, ymin), (xmax, ymax), color=(0, 255, 0), thickness=2)

    if pose_result.pose_landmarks:
        print(
            f'Nose coordinates: ('
            f'{pose_result.pose_landmarks.landmark[mp_pose.PoseLandmark.NOSE].x * im_w}, '
            f'{pose_result.pose_landmarks.landmark[mp_pose.PoseLandmark.NOSE].y * im_h})'
        )
        if save:    
            annotated_image = image.copy()
            mp_drawing.draw_landmarks(
                annotated_image,
                pose_result.pose_landmarks,
                mp_pose.POSE_CONNECTIONS,
                landmark_drawing_spec=mp_drawing_styles.get_default_pose_landmarks_style())
            
            cv2.imwrite(output_filename, annotated_image)
    else:
        print('Nose coordinates: not found')
        if save:
            cv2.imwrite(output_filename, image)
            
    return pose_result.pose_landmarks, ball_boxes
    

NEEDED_ANGLES = {
    'head_1': (0, 12, 24),
    'head_2': (0, 11, 23),
    
    'head_hands': (0, 15, 16),
    
    'left_elbow'     : (13, 11, 15),
    # 'right_elbow'    : (14, 12, 16),
    
    'left_shoulder'  : (11, 13, 23),
    'right_shoulder' : (12, 14, 24),
    
    'left_hip'  : (23, 11, 25),
    'right_hip' : (24, 12, 26),
    
    # 'left_knee'  : (25, 23, 27),
    'right_knee' : (26, 24, 28),
    
    # 'leg_open_1' : (24, 26, 25),
    # 'leg_open_2' : (23, 26, 25),
}

def get_needed_angles_from_pose_results(pose_results):
    all_frames_angles = []
    for pose_result in pose_results:
        if pose_result is None:
            all_frames_angles.append(None)
            continue
        
        one_frame_angles = dict()
        for position, point_indice in NEEDED_ANGLES.items():
            
            points = []
            visibilities = []
            for idx in point_indice:
                landmark_point = pose_result.landmark[idx]
                point = (landmark_point.x, landmark_point.y, landmark_point.z)
                points.append(point)
                visibilities.append(landmark_point.visibility)
            
            one_frame_angles[position] = {'angle':get_angle_at(*points), 'visibility':np.array(visibilities)}
            
        all_frames_angles.append(one_frame_angles)
    return all_frames_angles
    
def get_angle_at(angle_point, p1, p2):
    v1 = np.array(tuple(val1-val2 for val1, val2 in zip(p1, angle_point)))
    v2 = np.array(tuple(val1-val2 for val1, val2 in zip(p2, angle_point)))
    
    cosTh = np.dot(v1, v2)
    sinTh = np.cross(v1, v2)
    angle3d = np.rad2deg(np.arctan2(sinTh,cosTh))+180
    return angle3d

def process_pose_results(pose_results):
    all_frame_angles = get_needed_angles_from_pose_results(pose_results)
    
    total_frame = len(all_frame_angles)
    for frame_idx, needed_angles in enumerate(all_frame_angles):
        if needed_angles is None:
            
            if frame_idx-1>=0 and all_frame_angles[frame_idx-1] is not None:
                previous_angles = all_frame_angles[frame_idx-1]
            else:
                 previous_angles = None
        
            if frame_idx+1<total_frame and all_frame_angles[frame_idx+1] is not None:
                next_angles = all_frame_angles[frame_idx+1]
            else:
                next_angles = None
             
            if previous_angles is None and next_angles is None:
                continue
            elif previous_angles is not None:
                #* 可以覆蓋的方式
                continue
            elif next_angles is not None:
                #* 可以覆蓋的方式
                continue
            else:
                #* 前後兩幀平均
                needed_angles = dict()
                for position in NEEDED_ANGLES.keys():

                    previous_angle3d = previous_angles[position]['angle']
                    previous_visibilities = previous_angles[position]['visibility']
                    
                    next_angle3d = next_angles[position]['angle']
                    next_visibilities = next_angles[position]['visibility']
                    needed_angles[position] = {
                        'angle'      : (previous_angle3d+next_angle3d)/2,
                        'visibility' : (previous_visibilities+next_visibilities)/2,
                    }
                all_frame_angles[frame_idx] = needed_angles
        
    return all_frame_angles

def get_frame_similarity(frame_angles_1, frame_angles_2, valid_visibility=0):
    if frame_angles_1 is None or frame_angles_2 is None:
        return 0
    
    all_similarities = []
    for position in NEEDED_ANGLES.keys():
        v1_1, v1_2, v1_3 = frame_angles_1[position]['visibility']
        v2_1, v2_2, v2_3 = frame_angles_2[position]['visibility']
        if v1_1>=valid_visibility and v1_2>=valid_visibility and v1_3>=valid_visibility and v2_1>=valid_visibility and v2_2>=valid_visibility and v2_3>=valid_visibility:
            pos_angle_1 = frame_angles_1[position]['angle']
            pos_angle_2 = frame_angles_2[position]['angle']
            
            similarity = np.dot(pos_angle_1, pos_angle_2) / np.linalg.norm(pos_angle_1) / np.linalg.norm(pos_angle_2)
            all_similarities.append(similarity)
    return np.mean(all_similarities)

def get_output_dir(videofilestr):
    videofile = Path(videofilestr)
    if not videofile.exists():
        raise RuntimeError(f'videofile: {str(videofile.absolute())} does not exist.')
    output_dir = Path('./output/', videofile.relative_to('data').parent, videofile.stem)
    return output_dir

def transform_video(videofilestr:str, save=True):
    output_dir = get_output_dir(videofilestr)
    
    cap = cv2.VideoCapture(str(videofilestr))
    if save and not output_dir.is_dir():
        output_dir.mkdir(parents=True, exist_ok=True)
    
    im_h, im_w = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)), int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    ball_detector = BallDetector(im_h, im_w, 3)

    pose_results = []
    ball_results = []
    with mp_pose.Pose(
        static_image_mode=True,
        model_complexity=2,
        enable_segmentation=True,
        min_detection_confidence=0.5) as pose_model:

        ret, image = cap.read()
        im_idx = 0
        while ret:
            pose_result, ball_result = process_one_image(image, im_h, im_w, pose_model, ball_detector, str(output_dir.joinpath(f'frame_{im_idx}.png')), save=save)
            pose_results.append(pose_result)
            ball_results.append(ball_result)
            
            # mp_drawing.plot_landmarks(
            #     results.pose_world_landmarks, mp_pose.POSE_CONNECTIONS)
            im_idx += 1
            ret, image = cap.read()
        cap.release()
    if save:
        with open(str(output_dir.joinpath(f'raw_pose_results_{Path(videofilestr).stem}.pkl')), 'wb+', ) as f:
            pickle.dump(pose_results, f)
            
        with open(str(output_dir.joinpath(f'raw_ball_results_{Path(videofilestr).stem}.pkl')), 'wb+', ) as f:
            pickle.dump(ball_results, f)
            
    return pose_results, ball_results

    
def calculate_avg_frames_similarity(v1_need_frames, v2_need_frames, valid_similarity):
    all_sims = []
    min_len = min(len(v1_need_frames), len(v2_need_frames))
    
    for v1_frame, v2_frame in zip(v1_need_frames[-min_len:], v2_need_frames[-min_len:]):
        sim = get_frame_similarity(v1_frame, v2_frame, valid_similarity)
        # print(sim)
        all_sims.append(sim)
        
    # avg_similarity = (np.mean(all_sims) + 1)/2
    avg_similarity = np.mean(all_sims) 
    # print('average similrity', avg_similarity)
    return avg_similarity
    
def get_needed_frames(videofilestr, key, key_range, save=False):
    if save:
        pose_results, ball_results = transform_video(videofilestr, save)
    else:
        pose_result_path = get_output_dir(videofilestr).joinpath(f'raw_pose_results_{Path(videofilestr).stem}.pkl')
        ball_result_path = get_output_dir(videofilestr).joinpath(f'raw_ball_results_{Path(videofilestr).stem}.pkl')
        with open(str(pose_result_path), 'rb+') as f:
            pose_results = pickle.load(f)
            
        with open(str(ball_result_path), 'rb+', ) as f:
            ball_results = pickle.load(f)
            
    processed_pose_results = process_pose_results(pose_results)
    
    key_from = key-(key_range-1)
    return processed_pose_results[key_from if key_from>=0 else 0:key+1]

def calculate_total_avg_similarities(key_range, valid_visibility, save):
    
    # coach videos
    videofile = './data/coach/overhand-serve/side7.mp4'
    c1_need_frames = get_needed_frames(videofile, 72, key_range, save)
    
    videofile = './data/coach/overhand-serve/side13.mp4'
    c2_need_frames = get_needed_frames(videofile, 52, key_range, save)
    
    videofile = './data/coach/overhand-serve/side1.mp4'
    c3_need_frames = get_needed_frames(videofile, 24, key_range, save)

    videofile = './data/coach/overhand-serve/side4.mp4'
    c4_need_frames = get_needed_frames(videofile, 51, key_range, save)
    
    videofile = './data/coach/overhand-serve/side15.mp4'
    c5_need_frames = get_needed_frames(videofile, 46, key_range, save)
    
    videofile = './data/coach/overhand-serve/side16.mp4'
    c6_need_frames = get_needed_frames(videofile, 63, key_range, save)
    
    coach_videos = [
        c1_need_frames,
        c2_need_frames,
        c3_need_frames,
        c4_need_frames,
        c5_need_frames,
        c6_need_frames,
    ]
    # normal videos
    videofile = './data/normal/overhand-serve/side1.mp4'
    n1_need_frames = get_needed_frames(videofile, 70, key_range, save)
    
    videofile = './data/normal/overhand-serve/side2.mp4'
    n2_need_frames = get_needed_frames(videofile, 38, key_range, save)
    
    videofile = './data/normal/overhand-serve/side3.mp4'
    n3_need_frames = get_needed_frames(videofile, 44, key_range, save)
    
    videofile = './data/normal/overhand-serve/side4.mp4'
    n4_need_frames = get_needed_frames(videofile, 33, key_range, save)
    
    videofile = './data/normal/overhand-serve/side5.mp4'
    n5_need_frames = get_needed_frames(videofile, 89, key_range, save)
    
    videofile = './data/normal/overhand-serve/side6.mp4'
    n6_need_frames = get_needed_frames(videofile, 31, key_range, save)
    
    normal_videos = [
        n1_need_frames,
        n2_need_frames,
        n3_need_frames,
        n4_need_frames,
        n5_need_frames,
        n6_need_frames,
    ]
    
    # calculate coach similarities
    coach_similarities = []
    for ci, cv in enumerate(coach_videos):
        for ref_i in range(ci+1, len(coach_videos)):
            coach_similarities.append(calculate_avg_frames_similarity(coach_videos[ci], coach_videos[ref_i], valid_visibility))
    
    # calculate coach to normal
    normal_similarities = []
    for cv in coach_videos:
        for nv in normal_videos:   
            normal_similarities.append(calculate_avg_frames_similarity(cv, nv, valid_visibility))
        
    # print(f'num_couch: {len(coach_videos)}, num_normal: {len(normal_videos)}, key_range: {key_range} valid_visibility: {valid_visibility}')
    mean_coach  = np.mean(coach_similarities)
    mean_normal = np.mean(normal_similarities)
    # print('coach  total average similarities', mean_coach)
    # print('normal total average similarities', mean_normal)

    # plt.figure(figsize=(15, 5))
    # plt.subplot(211)
    # plt.title('Coach Video')
    # for position in NEEDED_ANGLES.keys():
    #     hw, hu, hv = [], [], []
    #     for frame in c1_need_frames:
    #         angels = frame[position]['angle']
            
    #         hw.append(angels[0])
    #         hu.append(angels[1])
    #         hv.append(angels[2])
        
    #     plt.xlabel('frame')
    #     plt.ylabel('angle')
    #     plt.plot(hw, label=position)
    #     # plt.plot(hu)
    #     # plt.plot(hv)
 
    # plt.legend(bbox_to_anchor=(0.995, 0.58))
    
    # plt.subplot(212)
    # plt.title('Normal Video')
    # for position in NEEDED_ANGLES.keys():
    #     hw, hu, hv = [], [], []
    #     for frame in n1_need_frames:
    #         angels = frame[position]['angle']
            
    #         hw.append(angels[0])
    #         hu.append(angels[1])
    #         hv.append(angels[2])
        
    #     plt.xlabel('frame')
    #     plt.ylabel('angle')
    #     plt.plot(hw, label=position)
    #     # plt.plot(hu)
    #     # plt.plot(hv)
 
    # plt.tight_layout(h_pad=2)
    # plt.show()
    
    return mean_coach, mean_normal
    

if __name__ == "__main__":
    # videofile = './data/testforyolo.mp4'
    
    #* 角度消融實驗
    # ORIGIN_NEEDED_ANGLES = NEEDED_ANGLES.copy()
    # for position in ORIGIN_NEEDED_ANGLES.keys():
    #     NEEDED_ANGLES = ORIGIN_NEEDED_ANGLES.copy()
    #     del NEEDED_ANGLES[position]
    #     print(f'DELETE: {position} {NEEDED_ANGLES.keys() = }')
        
    #     save = False
    #     key_ranges = [1, 5, 10, 15, 20, 25, 30]
    #     # key_ranges = [30]
    #     valid_visibilities = np.arange(10)/10
        
    #     total_mc, total_mn = [], []
    #     for valid_visibility in valid_visibilities:
            
    #         for key_range in key_ranges:
    #             mc, mn = calculate_total_avg_similarities(key_range, valid_visibility, save)
    #             total_mc.append(mc)
    #             total_mn.append(mn)
    #         #     break
    #         # break
    #         # print('-'*20)
        
    #     MC = np.mean(total_mc)
    #     MN = np.mean(total_mn)
    #     print(f'total_mc: {MC} total_mn: {MN} total_diff: {MC-MN}')
    
    # 原本角度的實驗
    print(f'{NEEDED_ANGLES.keys() = }')
    
    save = False
    # key_ranges = [1, 5, 10, 15, 20, 25, 30]
    key_ranges = [30]
    valid_visibilities = np.arange(10)/10
    
    for valid_visibility in valid_visibilities:
        total_mc, total_mn = [], []
        
        for key_range in key_ranges:
            mc, mn = calculate_total_avg_similarities(key_range, valid_visibility, save)
            total_mc.append(mc)
            total_mn.append(mn)
    
        MC = np.mean(total_mc)
        MN = np.mean(total_mn)
        
        print(f"{valid_visibility = }")
        print(f'total_mc: {MC} total_mn: {MN} total_diff: {MC-MN}')
        print('-'*20)
    
    
    # from glob import glob
    # for videofile in glob('data/overhand-serve/*'):
    #     transform_video(videofile, save)
        
    

